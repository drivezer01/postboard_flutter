import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:postboard_flutter/screens/edit-profile.screen.dart';
import 'package:postboard_flutter/screens/board-form.screen.dart';
import 'screens/board-detail.screen.dart';
import 'screens/board.screen.dart';
import 'screens/chat-detail.screen.dart';
import 'screens/chat.screen.dart';
import 'utilities/colors.dart';
import 'widgets/bottom-menu.dart';
import 'screens/account.screen.dart';
import 'screens/login.screen.dart';
import 'screens/main.screen.dart';
import 'screens/register.screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: [SystemUiOverlay.bottom]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: const ColorScheme.light(primary: AppColors.primaryColor),
        primaryColor: AppColors.primaryColor,
        buttonTheme: const ButtonThemeData(buttonColor: AppColors.primaryColor),
        inputDecorationTheme: const InputDecorationTheme(
          labelStyle: TextStyle(
            color: AppColors.textFieldColor,
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: AppColors.textFieldColor,
            ),
          ),
        ),
        textSelectionTheme:
            const TextSelectionThemeData(cursorColor: AppColors.textFieldColor),
      ),
      home: const MainScreen(),
      routes: {
        BottomNavBar.routeName: (context) => const BottomNavBar(),
        AccountScreen.routeName: (context) => const AccountScreen(),
        BoardScreen.routeName: (context) => const BoardScreen(),
        LoginScreen.routeName: (context) => const LoginScreen(),
        MainScreen.routeName: (context) => const MainScreen(),
        RegisterScreen.routeName: (context) => const RegisterScreen(),
        EditProfileScreen.routeName: (context) => const EditProfileScreen(),
        BoardFormScreen.routeName: (context) => const BoardFormScreen(),
        BoardDetailScreen.routeName: (context) => const BoardDetailScreen(),
        ChatScreen.routeName: (context) => const ChatScreen(),
        ChatDetailScreen.routeName: (context) => const ChatDetailScreen(),
      },
    );
  }
}

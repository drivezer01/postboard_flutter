import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:postboard_flutter/utilities/colors.dart';

class CustomWidgets {
  static PreferredSizeWidget appBar(
      BuildContext context, String title, List<Widget> actionItems) {
    return PreferredSize(
      preferredSize: const Size.fromHeight(50),
      child: AppBar(
          backgroundColor: AppColors.primaryColor,
          elevation: 0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: () => Navigator.pop(context),
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
          ),
          title: Text(title, style: const TextStyle(color: Colors.black)),
          actions: actionItems),
    );
  }

  static Widget loading(BuildContext context, double radius) {
    return Container(
        child: Center(child: CupertinoActivityIndicator(radius: radius)));
  }
}

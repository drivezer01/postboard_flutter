// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:postboard_flutter/services/auth.service.dart';
import 'package:postboard_flutter/utilities/colors.dart';
import 'package:postboard_flutter/utilities/constants.dart';
import 'package:postboard_flutter/widgets/bottom-menu.dart';
import 'login.screen.dart';
import 'register.screen.dart';

class MainScreen extends StatefulWidget {
  static const routeName = '/main';

  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  void initState() {
    super.initState();
    onInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void onInit() {
    AuthService().getRemember().then((remember) {
      if (remember == 'true') {
        Navigator.of(context).pushReplacementNamed(BottomNavBar.routeName);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Expanded(
            child: Container(
                color: AppColors.primaryColor,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 100,
                      child: const Center(
                          child: Text('Match Truck',
                              style: TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.bold))),
                    ),
                    SizedBox(
                        width: 300,
                        height: 300,
                        child: FittedBox(
                          child: Image.asset('assets/logo.png'),
                          fit: BoxFit.fill,
                        ))
                  ],
                ))),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            OutlinedButton(
                style: Constants.outlineButtonStyle(null, null),
                onPressed: () => Navigator.of(context)
                    .pushNamed(LoginScreen.routeName)
                    .whenComplete(() => onInit()),
                child: Container(
                    width: 80,
                    alignment: Alignment.center,
                    child: const Text('LOGIN',
                        style: TextStyle(color: Colors.black)))),
            const SizedBox(width: 20),
            OutlinedButton(
                style: Constants.outlineButtonStyle(null, null),
                onPressed: () =>
                    Navigator.of(context).pushNamed(RegisterScreen.routeName),
                child: Container(
                    width: 80,
                    alignment: Alignment.center,
                    child: const Text('REGISTER',
                        style: TextStyle(color: Colors.black)))),
          ],
        )
      ],
    ));
  }
}

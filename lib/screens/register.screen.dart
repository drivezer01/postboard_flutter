import 'package:flutter/material.dart';
import 'package:postboard_flutter/models/user.model.dart';
import 'package:postboard_flutter/services/user.service.dart';
import 'package:postboard_flutter/utilities/constants.dart';
import 'package:postboard_flutter/widgets/custom-widgets.dart';
import 'package:postboard_flutter/widgets/toasts.dart';

class RegisterScreen extends StatefulWidget {
  static const routeName = '/register';

  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _cPasswordController = TextEditingController();
  final TextEditingController _firstnameController = TextEditingController();
  final TextEditingController _lastnameController = TextEditingController();

  final UserModel _user = UserModel();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomWidgets.appBar(context, 'Register', []),
        body: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Container(
            color: Colors.transparent,
            child: Column(
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  child: const Center(
                      child: Text('Match Truck',
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.bold))),
                ),
                SizedBox(
                    width: 200,
                    height: 200,
                    child: FittedBox(
                      child: Image.asset('assets/logo.png'),
                      fit: BoxFit.fill,
                    )),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: SizedBox(
                        width: (MediaQuery.of(context).size.width / 2) - 20,
                        height: 40,
                        child: TextFormField(
                          controller: _firstnameController,
                          onChanged: (value) => setState(() {
                            _user.userFirstname = value.toString();
                          }),
                          decoration: const InputDecoration(
                            errorStyle: TextStyle(height: 0),
                            labelText: 'Firstname',
                            hintText: 'Firstname',
                            hintStyle: TextStyle(fontSize: 12),
                            border: OutlineInputBorder(),
                          ).copyWith(isDense: true),
                          style: const TextStyle(fontSize: 14),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: SizedBox(
                        width: (MediaQuery.of(context).size.width / 2) - 20,
                        height: 40,
                        child: TextFormField(
                          controller: _lastnameController,
                          onChanged: (value) => setState(() {
                            _user.userLastname = value.toString();
                          }),
                          decoration: const InputDecoration(
                            errorStyle: TextStyle(height: 0),
                            labelText: 'Lastname',
                            hintText: 'Lastname',
                            hintStyle: TextStyle(fontSize: 12),
                            border: OutlineInputBorder(),
                          ).copyWith(isDense: true),
                          style: const TextStyle(fontSize: 14),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: _emailController,
                      onChanged: (value) => setState(() {
                        _user.userEmail = value.toString();
                      }),
                      decoration: const InputDecoration(
                        errorStyle: TextStyle(height: 0),
                        labelText: 'Email',
                        hintText: 'Email',
                        hintStyle: TextStyle(fontSize: 12),
                        border: OutlineInputBorder(),
                      ).copyWith(isDense: true),
                      style: const TextStyle(fontSize: 14),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: SizedBox(
                        width: (MediaQuery.of(context).size.width / 2) - 20,
                        height: 40,
                        child: TextFormField(
                          controller: _passwordController,
                          onChanged: (value) => setState(() {
                            _user.userPassword = value.toString();
                          }),
                          obscureText: true,
                          decoration: const InputDecoration(
                            errorStyle: TextStyle(height: 0),
                            labelText: 'Password',
                            hintText: 'Password',
                            hintStyle: TextStyle(fontSize: 12),
                            border: OutlineInputBorder(),
                          ).copyWith(isDense: true),
                          style: const TextStyle(fontSize: 14),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: SizedBox(
                        width: (MediaQuery.of(context).size.width / 2) - 20,
                        height: 40,
                        child: TextFormField(
                          controller: _cPasswordController,
                          obscureText: true,
                          decoration: const InputDecoration(
                            errorStyle: TextStyle(height: 0),
                            labelText: 'Confirm Password',
                            hintText: 'Confirm Password',
                            hintStyle: TextStyle(fontSize: 12),
                            border: OutlineInputBorder(),
                          ).copyWith(isDense: true),
                          style: const TextStyle(fontSize: 14),
                        ),
                      ),
                    ),
                  ],
                ),
                OutlinedButton(
                    style: Constants.outlineButtonStyle(null, null),
                    onPressed: () => onRegister(),
                    child: Container(
                        width: 80,
                        alignment: Alignment.center,
                        child: const Text('REGISTER',
                            style: TextStyle(color: Colors.black)))),
              ],
            ),
          ),
        ));
  }

  Future<void> onRegister() async {
    FocusScope.of(context).unfocus();
    Future.delayed(const Duration(milliseconds: 500), () async {
      if (_cPasswordController.text == _user.userPassword &&
          _user.userPassword != '') {
        if (_user.userFirstname != '' && _user.userEmail != '') {
          await UserService().create(_user).then((value) {
            Toasts.toastSuccess(context, 'สมัครใช้งานสำเร็จ', 1);
            Navigator.of(context).pop();
          }).catchError((ex) {
            print(ex);
            Toasts.toastError(context, 'เกิดข้อผิดพลาด', 1);
          });
        } else {
          Toasts.toastWarning(context, 'กรอกข้อมูลไม่ครบ', 1);
        }
      } else {
        Toasts.toastWarning(context, 'รหัสผ่านไม่ตรงกัน', 1);
      }
    });
  }
}

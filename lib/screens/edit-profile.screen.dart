import 'dart:convert' as convert;
import 'package:flutter/material.dart';
import 'package:postboard_flutter/models/user.model.dart';
import 'package:postboard_flutter/services/auth.service.dart';
import 'package:postboard_flutter/services/upload.service.dart';
import 'package:postboard_flutter/services/user.service.dart';
import 'package:postboard_flutter/utilities/constants.dart';
import 'package:postboard_flutter/utilities/globals.dart';
import 'package:postboard_flutter/widgets/custom-widgets.dart';
import 'package:postboard_flutter/widgets/toasts.dart';
import 'package:image_picker/image_picker.dart';

class EditProfileScreen extends StatefulWidget {
  static const routeName = '/edit-profile';

  const EditProfileScreen({Key? key}) : super(key: key);

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  bool isLoading = true;
  UserModel _user = UserModel();
  String userId = '';
  final ImagePicker _picker = ImagePicker();

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _cPasswordController = TextEditingController();
  final TextEditingController _firstnameController = TextEditingController();
  final TextEditingController _lastnameController = TextEditingController();

  @override
  void initState() {
    super.initState();
    onInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> onInit() async {
    userId = await AuthService().decodeUserId();
    UserService().findOne(userId).then((res) => setState(() {
          _user = res;
          _emailController.text = res.userEmail.toString();
          _firstnameController.text = res.userFirstname.toString();
          _lastnameController.text = res.userLastname.toString();

          _passwordController.text = '';
          _cPasswordController.text = '';
          isLoading = false;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? CustomWidgets.loading(context, 10)
        : Scaffold(
            appBar: CustomWidgets.appBar(context, 'Edit Profile', []),
            body: GestureDetector(
              onTap: () => FocusScope.of(context).unfocus(),
              child: Container(
                color: Colors.transparent,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: GestureDetector(
                        onTap: () => selectImage(),
                        child: CircleAvatar(
                          radius: 40,
                          backgroundColor: Colors.grey[200],
                          child: CircleAvatar(
                            radius: 37,
                            backgroundColor: Colors.white,
                            backgroundImage: NetworkImage(
                                '${Globals.API}/uploads/profile/${_user.userThumbnail}'),
                          ),
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: SizedBox(
                            width: (MediaQuery.of(context).size.width / 2) - 20,
                            height: 40,
                            child: TextFormField(
                              controller: _firstnameController,
                              onChanged: (value) => setState(() {
                                _user.userFirstname = value.toString();
                              }),
                              decoration: const InputDecoration(
                                errorStyle: TextStyle(height: 0),
                                labelText: 'Firstname',
                                hintText: 'Firstname',
                                hintStyle: TextStyle(fontSize: 12),
                                border: OutlineInputBorder(),
                              ).copyWith(isDense: true),
                              style: const TextStyle(fontSize: 14),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: SizedBox(
                            width: (MediaQuery.of(context).size.width / 2) - 20,
                            height: 40,
                            child: TextFormField(
                              controller: _lastnameController,
                              onChanged: (value) => setState(() {
                                _user.userLastname = value.toString();
                              }),
                              decoration: const InputDecoration(
                                errorStyle: TextStyle(height: 0),
                                labelText: 'Lastname',
                                hintText: 'Lastname',
                                hintStyle: TextStyle(fontSize: 12),
                                border: OutlineInputBorder(),
                              ).copyWith(isDense: true),
                              style: const TextStyle(fontSize: 14),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: SizedBox(
                        height: 40,
                        child: TextFormField(
                          controller: _emailController,
                          onChanged: (value) => setState(() {
                            _user.userEmail = value.toString();
                          }),
                          decoration: const InputDecoration(
                            errorStyle: TextStyle(height: 0),
                            labelText: 'Email',
                            hintText: 'Email',
                            hintStyle: TextStyle(fontSize: 12),
                            border: OutlineInputBorder(),
                          ).copyWith(isDense: true),
                          style: const TextStyle(fontSize: 14),
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(top: 20),
                      child: const Text('** ถ้าไม่เปลี่ยนรหัสไม่ต้องกรอก **'),
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: SizedBox(
                            width: (MediaQuery.of(context).size.width / 2) - 20,
                            height: 40,
                            child: TextFormField(
                              controller: _passwordController,
                              onChanged: (value) => setState(() {
                                _user.userPassword = value.toString();
                              }),
                              obscureText: true,
                              decoration: const InputDecoration(
                                errorStyle: TextStyle(height: 0),
                                labelText: 'Password',
                                hintText: 'Password',
                                hintStyle: TextStyle(fontSize: 12),
                                border: OutlineInputBorder(),
                              ).copyWith(isDense: true),
                              style: const TextStyle(fontSize: 14),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: SizedBox(
                            width: (MediaQuery.of(context).size.width / 2) - 20,
                            height: 40,
                            child: TextFormField(
                              controller: _cPasswordController,
                              obscureText: true,
                              decoration: const InputDecoration(
                                errorStyle: TextStyle(height: 0),
                                labelText: 'Confirm Password',
                                hintText: 'Confirm Password',
                                hintStyle: TextStyle(fontSize: 12),
                                border: OutlineInputBorder(),
                              ).copyWith(isDense: true),
                              style: const TextStyle(fontSize: 14),
                            ),
                          ),
                        ),
                      ],
                    ),
                    OutlinedButton(
                        style: Constants.outlineButtonStyle(
                            Colors.greenAccent, Colors.white),
                        onPressed: () {
                          if (_passwordController.text != '') {
                            if (_passwordController.text ==
                                _cPasswordController.text) {
                              onSave();
                            } else {
                              Toasts.toastWarning(
                                  context, 'รหัสผ่านไม่ตรงกัน', 2);
                            }
                          } else {
                            onSave();
                          }
                        },
                        child: Container(
                            width: 80,
                            alignment: Alignment.center,
                            child: const Text('SAVE',
                                style: TextStyle(color: Colors.black)))),
                  ],
                ),
              ),
            ));
  }

  Future<void> selectImage() async {
    FocusScope.of(context).unfocus();
    await _picker
        .pickImage(source: ImageSource.gallery)
        .then((res) async => await UploadService()
                .uploadImage(res?.path, 'profile')
                .then((upload) async {
              var responseData =
                  convert.jsonDecode(await upload!.stream.bytesToString());
              setState(() {
                if (upload.statusCode == 200) {
                  _user.userThumbnail = responseData['upload_name'];
                } else {
                  print(responseData);
                  Toasts.toastWarning(context, 'เกิดข้อผิดพลาด', 2);
                }
              });
            }).catchError((errUpload) {
              print(errUpload);
              Toasts.toastWarning(context, 'เกิดข้อผิดพลาด', 2);
            }))
        .catchError((err) {
      print(err);
      Toasts.toastWarning(context, 'ไม่ได้เลือกรูปภาพ', 2);
    });
  }

  Future<void> onSave() async {
    FocusScope.of(context).unfocus();
    Future.delayed(const Duration(seconds: 1), () {
      UserService().update(_user).then((value) {
        onInit();
        Toasts.toastSuccess(context, 'บันทึกข้อมูลสำเร็จ', 1);
      }).catchError((err) {
        print(err);
        Toasts.toastError(context, 'เกิดข้อผิดพลาด', 2);
      });
    });
  }
}

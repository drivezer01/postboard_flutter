import 'package:flutter/material.dart';
import 'package:postboard_flutter/models/chat.model.dart';
import 'package:postboard_flutter/services/auth.service.dart';
import 'package:postboard_flutter/services/chat.service.dart';
import 'package:postboard_flutter/utilities/globals.dart';
import 'package:postboard_flutter/widgets/custom-widgets.dart';

import 'chat-detail.screen.dart';

class ChatScreen extends StatefulWidget {
  static const routeName = '/chat';

  const ChatScreen({Key? key}) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  bool isLoading = true;
  String userId = '';

  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    onInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> onInit() async {
    userId = await AuthService().decodeUserId();
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? CustomWidgets.loading(context, 10)
        : Scaffold(
            body: Container(
            padding: const EdgeInsets.all(10),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: buildFutureBuilder(context),
          ));
  }

  Widget buildFutureBuilder(BuildContext context) {
    return FutureBuilder(
      future: ChatService().getChatList(userId),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return buildGridViewItems(snapshot.data);
        } else {
          return CustomWidgets.loading(context, 10);
        }
      },
    );
  }

  Widget buildGridViewItems(List<ChatWithUserModel> model) {
    if (model.isNotEmpty) {
      return ListView.builder(
          scrollDirection: Axis.vertical,
          controller: _scrollController,
          primary: false,
          itemCount: model.length,
          itemBuilder: (context, int index) => listDataItems(model[index]));
    } else {
      return Container();
    }
  }

  Widget listDataItems(ChatWithUserModel user) {
    return GestureDetector(
      onTap: () => Navigator.of(context)
          .pushNamed(ChatDetailScreen.routeName,
              arguments:
                  '${userId == user.chatReceived?.userId ? user.chatSender?.userId : user.chatReceived?.userId}')
          .whenComplete(() => setState(() {})),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              CircleAvatar(
                radius: 20,
                backgroundColor: Colors.grey[200],
                child: CircleAvatar(
                  radius: 17,
                  backgroundColor: Colors.white,
                  backgroundImage: NetworkImage(
                      '${Globals.API}/uploads/profile/${userId == user.chatReceived?.userId ? user.chatSender?.userThumbnail : user.chatReceived?.userThumbnail}'),
                ),
              ),
              const SizedBox(width: 5),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                        '${userId == user.chatReceived?.userId ? user.chatSender?.userFirstname : user.chatReceived?.userFirstname} ${userId == user.chatReceived?.userId ? user.chatSender?.userLastname : user.chatReceived?.userLastname}',
                        style: const TextStyle(fontSize: 10)),
                    Text(
                        '${userId == user.chatSender?.userId ? 'คุณส่ง:' : 'ตอบกลับ:'} ${user.chatText}')
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

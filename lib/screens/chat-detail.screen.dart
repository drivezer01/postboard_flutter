import 'dart:developer';
import 'dart:convert' as convert;
import 'package:flutter/material.dart';
import 'package:postboard_flutter/models/chat.model.dart';
import 'package:postboard_flutter/models/user.model.dart';
import 'package:postboard_flutter/services/auth.service.dart';
import 'package:postboard_flutter/services/chat.service.dart';
import 'package:postboard_flutter/services/user.service.dart';
import 'package:postboard_flutter/utilities/globals.dart';
import 'package:postboard_flutter/widgets/custom-widgets.dart';
import 'package:postboard_flutter/widgets/toasts.dart';

class ChatDetailScreen extends StatefulWidget {
  static const routeName = '/chat-detail';

  const ChatDetailScreen({Key? key}) : super(key: key);

  @override
  _ChatDetailScreenState createState() => _ChatDetailScreenState();
}

class _ChatDetailScreenState extends State<ChatDetailScreen> {
  bool isLoading = true;
  String userId = '';
  String receivedId = '';
  String senderId = '';
  String roomId = '';
  double pageHeight = 0;
  String query = '';

  ChatModel _chat = ChatModel();
  UserModel _userReceived = UserModel();
  UserModel _userSender = UserModel();

  ScrollController? _scrollController;
  final TextEditingController _textController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
  }

  @override
  void dispose() {
    _scrollController?.dispose();
    super.dispose();
  }

  Future<void> onInit() async {
    userId = await AuthService().decodeUserId();
    senderId = userId;
    query =
        '{"\$and":[{"\$or":[{"chat_sender_id":"$senderId"},{"chat_sender_id":"$receivedId"}]},{"\$or":[{"chat_received_id":"$receivedId"},{"chat_received_id":"$senderId"}]}]}';
    await ChatService().findAll(query).then((res) {
      if (res.length != 0) {
        _chat.chatRoom = res[res.length - 1].chatRoom.toString();
      }
    });
    await UserService().findOne(receivedId).then((res) => _userReceived = res);
    await UserService().findOne(senderId).then((res) => setState(() {
          _userSender = res;
          isLoading = false;
          Future.delayed(const Duration(milliseconds: 500), () {
            _scrollController?.animateTo(
                _scrollController!.position.maxScrollExtent,
                duration: const Duration(seconds: 1),
                curve: Curves.ease);
          });

          // if (_scrollController.hasClients) {
          //   Future.delayed(const Duration(milliseconds: 500), () {
          //     _singleChildScrollController.jumpTo(
          //         _singleChildScrollController.position.maxScrollExtent);
          //   });
          // }
          // _singleChildScrollController.animateTo(180.0,
          //     duration: const Duration(milliseconds: 500), curve: Curves.ease);
        }));
  }

  @override
  Widget build(BuildContext context) {
    Object? resItem = ModalRoute.of(context)?.settings.arguments;
    if (isLoading) {
      pageHeight = MediaQuery.of(context).size.height - 100;
      receivedId = resItem.toString();
      onInit();
    }

    // if (isLoading == false) {
    //   if (_scrollController!.hasClients) {
    //     _scrollController?.animateTo(
    //         _scrollController!.position.maxScrollExtent,
    //         duration: const Duration(microseconds: 500),
    //         curve: Curves.ease);
    //   }
    // }
    return Scaffold(
      appBar: CustomWidgets.appBar(context, 'Chat', []),
      body: isLoading
          ? CustomWidgets.loading(context, 10)
          : GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
                setState(() =>
                    pageHeight = MediaQuery.of(context).size.height - 100);
              },
              child: SingleChildScrollView(
                primary: false,
                child: Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: pageHeight,
                      color: Colors.transparent,
                      child: buildFutureBuilder(context),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 5),
                      child: SizedBox(
                        height: 40,
                        child: TextFormField(
                          controller: _textController,
                          onTap: () => setState(() => pageHeight =
                              MediaQuery.of(context).size.height - 350),
                          onFieldSubmitted: (value) => onSend(),
                          onChanged: (value) =>
                              _chat.chatText = value.toString(),
                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                                onPressed: () => onSend(),
                                icon: const Icon(Icons.send)),
                            errorStyle: const TextStyle(height: 0),
                            labelText: 'พิมพ์ข้อความ',
                            hintText: 'พิมพ์ข้อความ',
                            hintStyle: const TextStyle(fontSize: 12),
                            border: const OutlineInputBorder(),
                          ).copyWith(isDense: true),
                          style: const TextStyle(fontSize: 14),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
    );
  }

  Widget buildFutureBuilder(BuildContext context) {
    return FutureBuilder(
      future: ChatService().findAll(query),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return buildGridViewItems(snapshot.data);
        } else {
          return CustomWidgets.loading(context, 10);
        }
      },
    );
  }

  Widget buildGridViewItems(List<ChatModel> model) {
    if (model.isNotEmpty) {
      return ListView.builder(
          scrollDirection: Axis.vertical,
          controller: _scrollController,
          primary: false,
          itemCount: model.length,
          itemBuilder: (context, int index) => listDataItems(model[index]));
    } else {
      return Container();
    }
  }

  Widget listDataItems(ChatModel user) {
    return senderId == user.chatSenderId
        ? Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Card(
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                            '${_userSender.userFirstname} ${_userSender.userLastname}',
                            style: const TextStyle(fontSize: 10)),
                        Text(user.chatText ?? '')
                      ],
                    ),
                  ),
                ),
                const SizedBox(width: 5),
                CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.grey[200],
                  child: CircleAvatar(
                    radius: 17,
                    backgroundColor: Colors.white,
                    backgroundImage: NetworkImage(
                        '${Globals.API}/uploads/profile/${_userSender.userThumbnail}'),
                  ),
                ),
              ],
            ),
          )
        : Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.grey[200],
                  child: CircleAvatar(
                    radius: 17,
                    backgroundColor: Colors.white,
                    backgroundImage: NetworkImage(
                        '${Globals.API}/uploads/profile/${_userReceived.userThumbnail}'),
                  ),
                ),
                const SizedBox(width: 5),
                Card(
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            '${_userReceived.userFirstname} ${_userReceived.userLastname}',
                            style: const TextStyle(fontSize: 10)),
                        Text(user.chatText ?? '')
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
  }

  Future<void> onSend() async {
    FocusScope.of(context).unfocus();
    setState(() => pageHeight = MediaQuery.of(context).size.height - 100);
    _chat.chatReceivedId = receivedId;
    _chat.chatSenderId = senderId;
    ChatService()
        .create(_chat)
        .then((value) => setState(() {
              var responseData = convert.jsonDecode(value!.body);
              _textController.text = '';
              roomId = responseData['chat_room'];
            }))
        .catchError((err) {
      print(err);
      Toasts.toastError(context, 'เกิดข้อผิดพลาด', 2);
    });
  }
}

import 'package:flutter/material.dart';
import 'package:postboard_flutter/models/post.model.dart';
import 'package:postboard_flutter/services/post.service.dart';
import 'package:postboard_flutter/utilities/colors.dart';
import 'package:postboard_flutter/utilities/globals.dart';
import 'package:postboard_flutter/widgets/custom-widgets.dart';

import 'board-detail.screen.dart';
import 'board-form.screen.dart';

class BoardScreen extends StatefulWidget {
  static const routeName = '/board';

  const BoardScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<BoardScreen> {
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    onInit();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Future<void> onInit() async {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: buildFutureBuilder(context),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: AppColors.primaryColor,
        onPressed: () => Navigator.of(context)
            .pushNamed(BoardFormScreen.routeName)
            .whenComplete(() => setState(() {})),
        child: const Icon(Icons.add),
      ),
    );
  }

  Widget buildFutureBuilder(BuildContext context) {
    return FutureBuilder(
      future: PostService().findAll(),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return buildGridViewItems(snapshot.data);
        } else {
          return CustomWidgets.loading(context, 10);
        }
      },
    );
  }

  Widget buildGridViewItems(List<PostWithUserModel> model) {
    if (model.isNotEmpty) {
      return ListView.builder(
          scrollDirection: Axis.vertical,
          controller: _scrollController,
          primary: false,
          itemCount: model.length,
          itemBuilder: (context, int index) => listDataItems(model[index]));
    } else {
      return Container();
    }
  }

  Widget listDataItems(PostWithUserModel user) {
    return GestureDetector(
      onTap: () => Navigator.of(context)
          .pushNamed(BoardDetailScreen.routeName, arguments: user.postId)
          .whenComplete(() => setState(() {})),
      child: Card(
        child: Container(
            padding: const EdgeInsets.all(10),
            height: 160,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(
                  radius: 30,
                  backgroundColor: Colors.grey[200],
                  child: CircleAvatar(
                    radius: 27,
                    backgroundColor: Colors.white,
                    backgroundImage: NetworkImage(
                        '${Globals.API}/uploads/profile/${user.userThumbnail}'),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width - 150,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            const Text('โพสโดย: ',
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            Text('${user.userFirstname} ${user.userLastname}',
                                overflow: TextOverflow.ellipsis),
                          ],
                        ),
                        Row(
                          children: [
                            const Text('รายละเอียดรถ: ',
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            Text(
                              '${user.postCarDetail}',
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            const Text('เขต/พื้นที่: ',
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            Text('${user.postLocation}',
                                overflow: TextOverflow.ellipsis),
                          ],
                        ),
                        const Text('รายละเอียดข้อมูล',
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        SizedBox(
                            width: MediaQuery.of(context).size.width - 150,
                            height: 50,
                            child: Text(
                              '${user.postDetail}',
                              overflow: TextOverflow.fade,
                            )),
                      ],
                    ),
                  ),
                )
              ],
            )),
      ),
    );
  }
}

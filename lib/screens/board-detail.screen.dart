import 'package:flutter/material.dart';
import 'package:postboard_flutter/models/post-image.model.dart';
import 'package:postboard_flutter/models/post.model.dart';
import 'package:postboard_flutter/models/user.model.dart';
import 'package:postboard_flutter/services/auth.service.dart';
import 'package:postboard_flutter/services/post-image.service.dart';
import 'package:postboard_flutter/services/post.service.dart';
import 'package:postboard_flutter/services/user.service.dart';
import 'package:postboard_flutter/utilities/globals.dart';
import 'package:postboard_flutter/widgets/custom-widgets.dart';
import 'board-form.screen.dart';
import 'dart:convert' as convert;

import 'chat-detail.screen.dart';

class BoardDetailScreen extends StatefulWidget {
  static const routeName = '/board-detail';

  const BoardDetailScreen({Key? key}) : super(key: key);

  @override
  _BoardDetailScreenState createState() => _BoardDetailScreenState();
}

class _BoardDetailScreenState extends State<BoardDetailScreen> {
  final ScrollController _scrollController = ScrollController();

  bool isLoading = true;
  String userId = '';
  String editId = '';
  UserModel _user = UserModel();
  PostModel _post = PostModel();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> onInit() async {
    userId = await AuthService().decodeUserId();
    await PostService().findOne(editId).then((post) async {
      _post = post;
      await UserService()
          .findOne(post.postUserId ?? '')
          .then((user) async => setState(() {
                _user = user;
                isLoading = false;
              }));
    });
  }

  @override
  Widget build(BuildContext context) {
    Object? itemId = ModalRoute.of(context)?.settings.arguments;
    if (isLoading == true) {
      editId = itemId.toString();
      onInit();
    }
    return Scaffold(
        appBar: CustomWidgets.appBar(context, 'Detail Post', [
          if (userId == _post.postUserId)
            IconButton(
                onPressed: () => Navigator.of(context)
                    .pushNamed(BoardFormScreen.routeName, arguments: editId)
                    .whenComplete(() => onInit()),
                icon: const Icon(Icons.edit, color: Colors.black))
          else
            IconButton(
                onPressed: () => Navigator.of(context)
                    .pushNamed(ChatDetailScreen.routeName,
                        arguments: _post.postUserId)
                    .whenComplete(() => onInit()),
                icon: const Icon(Icons.chat_outlined, color: Colors.black))
        ]),
        body: isLoading
            ? CustomWidgets.loading(context, 10)
            : SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          CircleAvatar(
                            radius: 40,
                            backgroundColor: Colors.grey[200],
                            child: CircleAvatar(
                              radius: 37,
                              backgroundColor: Colors.white,
                              backgroundImage: NetworkImage(
                                  '${Globals.API}/uploads/profile/${_user.userThumbnail}'),
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          Text('${_user.userFirstname} ${_user.userLastname}'),
                        ],
                      ),
                      const SizedBox(height: 20),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text('รายละเอียดรถ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          Text('${_post.postCarDetail}'),
                          const SizedBox(height: 10),
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            child: SizedBox(
                                height: 200,
                                child: buildFutureBuilder(context)),
                          ),
                          const Text('เขต/พื้นที่',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          Text('${_post.postLocation}'),
                          const SizedBox(height: 10),
                          const Text('รายละเอียดข้อมูล',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          Text('${_post.postDetail}'),
                          const SizedBox(height: 10),
                          const Text('ช่องทางการติดต่อ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          Row(
                            children: [
                              const Text('เบอร์โทรศัพท์: ',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              Text('${_post.postPhone}'),
                            ],
                          ),
                          Row(
                            children: [
                              const Text('Line: ',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              Text('${_post.postLine}'),
                            ],
                          ),
                          Row(
                            children: [
                              const Text('Facebook: ',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              Text('${_post.postFacebook}'),
                            ],
                          ),
                          Row(
                            children: [
                              const Text('อื่นๆ: ',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              Text('${_post.postOther}'),
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ));
  }

  Widget buildFutureBuilder(BuildContext context) {
    return FutureBuilder(
      future:
          PostImageService().findAll('{"\$and":[{"image_post_id":"$editId"}]}'),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return buildGridViewItems(snapshot.data);
        } else {
          return CustomWidgets.loading(context, 10);
        }
      },
    );
  }

  Widget buildGridViewItems(List<PostImageModel> model) {
    if (model.isNotEmpty) {
      return ListView.builder(
          scrollDirection: Axis.horizontal,
          controller: _scrollController,
          primary: false,
          itemCount: model.length,
          itemBuilder: (context, int index) =>
              listDataItems(model[index].imageThumbnail));
    } else {
      return const Center(child: Text('ไม่มีรูปภาพ'));
    }
  }

  Widget listDataItems(String? name) {
    return Card(
      child: SizedBox(
        height: 200,
        child: FittedBox(
          child: Image.network("${Globals.API}/uploads/post/$name"),
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:postboard_flutter/services/auth.service.dart';
import 'package:postboard_flutter/utilities/constants.dart';
import 'package:postboard_flutter/widgets/custom-widgets.dart';
import 'package:postboard_flutter/widgets/toasts.dart';
import 'dart:convert' as convert;

class LoginScreen extends StatefulWidget {
  static const routeName = '/login';

  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomWidgets.appBar(context, 'Login', []),
        body: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Container(
            color: Colors.transparent,
            child: Column(
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  child: const Center(
                      child: Text('Match Truck',
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.bold))),
                ),
                SizedBox(
                    width: 200,
                    height: 200,
                    child: FittedBox(
                      child: Image.asset('assets/logo.png'),
                      fit: BoxFit.fill,
                    )),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: _emailController,
                      decoration: const InputDecoration(
                        errorStyle: TextStyle(height: 0),
                        labelText: 'Email',
                        hintText: 'Email',
                        hintStyle: TextStyle(fontSize: 12),
                        border: OutlineInputBorder(),
                      ).copyWith(isDense: true),
                      style: const TextStyle(fontSize: 14),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: SizedBox(
                    height: 40,
                    child: TextFormField(
                      controller: _passwordController,
                      obscureText: true,
                      decoration: const InputDecoration(
                        errorStyle: TextStyle(height: 0),
                        labelText: 'Password',
                        hintText: 'Password',
                        hintStyle: TextStyle(fontSize: 12),
                        border: OutlineInputBorder(),
                      ).copyWith(isDense: true),
                      style: const TextStyle(fontSize: 14),
                    ),
                  ),
                ),
                OutlinedButton(
                    style: Constants.outlineButtonStyle(null, null),
                    onPressed: () => onLogin(),
                    child: Container(
                        width: 80,
                        alignment: Alignment.center,
                        child: const Text('LOGIN',
                            style: TextStyle(color: Colors.black)))),
              ],
            ),
          ),
        ));
  }

  Future<void> onLogin() async {
    FocusScope.of(context).unfocus();
    Future.delayed(const Duration(milliseconds: 500), () async {
      if (_emailController.text != '' && _passwordController.text != '') {
        await AuthService()
            .login(_emailController.text, _passwordController.text)
            .then((value) {
          var responseData = convert.jsonDecode(value.body);
          if (value.statusCode == 201) {
            AuthService().setRemember('true');
            AuthService().setToken(responseData['access_token']);
            Navigator.of(context).pop();
            Toasts.toastSuccess(context, 'เข้าสู่ระบบสำเร็จ', 1);
          } else {
            Toasts.toastWarning(context, '${responseData['message']}', 2);
          }
        }).catchError((ex) {
          print(ex);
          Toasts.toastError(context, 'เกิดข้อผิดพลาด', 1);
        });
      } else {
        Toasts.toastWarning(context, 'กรอกข้อมูลไม่ครบ', 1);
      }
    });
  }
}

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:postboard_flutter/models/post-image.model.dart';
import 'package:postboard_flutter/models/post.model.dart';
import 'package:postboard_flutter/models/user.model.dart';
import 'package:postboard_flutter/services/auth.service.dart';
import 'package:postboard_flutter/services/post-image.service.dart';
import 'package:postboard_flutter/services/post.service.dart';
import 'package:postboard_flutter/services/upload.service.dart';
import 'package:postboard_flutter/services/user.service.dart';
import 'package:postboard_flutter/utilities/constants.dart';
import 'package:postboard_flutter/utilities/globals.dart';
import 'package:postboard_flutter/widgets/custom-widgets.dart';
import 'package:postboard_flutter/widgets/toasts.dart';
import 'dart:convert' as convert;

class BoardFormScreen extends StatefulWidget {
  static const routeName = '/board-form';

  const BoardFormScreen({Key? key}) : super(key: key);

  @override
  _BoardFormScreenState createState() => _BoardFormScreenState();
}

class _BoardFormScreenState extends State<BoardFormScreen> {
  bool isLoading = true;
  String userId = '';
  UserModel _user = UserModel();
  PostModel _post = PostModel();
  List<PostImageModel> _postImage = <PostImageModel>[];
  String editId = '';
  final List<String> _listImage = [];

  final ScrollController _scrollController = ScrollController();
  final ImagePicker _picker = ImagePicker();

  final TextEditingController _carDetailController = TextEditingController();
  final TextEditingController _locationController = TextEditingController();
  final TextEditingController _detailController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _lineController = TextEditingController();
  final TextEditingController _facebookController = TextEditingController();
  final TextEditingController _otherController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Future<void> onInit() async {
    _user = await UserService().findOne(_user.userId ?? '');
    setState(() => isLoading = false);
  }

  Future<void> onLoadEdit() async {
    _carDetailController.text = _post.postCarDetail.toString();
    _locationController.text = _post.postLocation.toString();
    _detailController.text = _post.postDetail.toString();
    _phoneController.text = _post.postPhone.toString();
    _lineController.text = _post.postLine.toString();
    _facebookController.text = _post.postFacebook.toString();
    _otherController.text = _post.postOther.toString();
    await PostImageService()
        .findAll('{"\$and":[{"image_post_id":"$editId"}]}')
        .then((value) {
      _postImage = value;
      for (PostImageModel item in value) {
        _listImage.add(item.imageThumbnail ?? '');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Object? itemId = ModalRoute.of(context)?.settings.arguments;
    if (isLoading == true) {
      editId = itemId.toString();
      if (itemId != null) {
        PostService().findOne(itemId.toString()).then((value) {
          _user.userId = value.postUserId;
          _post = value;
          onLoadEdit();
          onInit();
        });
      } else {
        AuthService().decodeUserId().then((value) {
          _user.userId = value;
          onInit();
        });
      }
    }

    return Scaffold(
        appBar: CustomWidgets.appBar(
            context, '${editId == 'null' ? 'Add' : 'Edit'} Post', [
          IconButton(
              onPressed: () => onSave(),
              icon: const Icon(Icons.check, color: Colors.black)),
        ]),
        body: isLoading
            ? CustomWidgets.loading(context, 10)
            : SingleChildScrollView(
                child: GestureDetector(
                    onTap: () => FocusScope.of(context).unfocus(),
                    child: Container(
                      color: Colors.transparent,
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              CircleAvatar(
                                radius: 40,
                                backgroundColor: Colors.grey[200],
                                child: CircleAvatar(
                                  radius: 37,
                                  backgroundColor: Colors.white,
                                  backgroundImage: NetworkImage(
                                      '${Globals.API}/uploads/profile/${_user.userThumbnail}'),
                                ),
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Text(
                                  '${_user.userFirstname} ${_user.userLastname}')
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            child: SizedBox(
                              height: 40,
                              child: TextFormField(
                                controller: _carDetailController,
                                onChanged: (value) => setState(() {
                                  _post.postCarDetail = value.toString();
                                }),
                                decoration: const InputDecoration(
                                  errorStyle: TextStyle(height: 0),
                                  labelText: 'รายละเอียดรถ',
                                  hintText: 'รายละเอียดรถ',
                                  hintStyle: TextStyle(fontSize: 12),
                                  border: OutlineInputBorder(),
                                ).copyWith(isDense: true),
                                style: const TextStyle(fontSize: 14),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            child: OutlinedButton(
                                style: Constants.outlineButtonStyle(
                                    Colors.white, Colors.black),
                                onPressed: () => selectImage(0, true),
                                child: Container(
                                    alignment: Alignment.center,
                                    child: const Text('เพิ่มรูปภาพ',
                                        style:
                                            TextStyle(color: Colors.black)))),
                          ),
                          if (_listImage.isNotEmpty)
                            Container(
                              alignment: Alignment.centerLeft,
                              padding: const EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 10),
                              child: SizedBox(
                                  height: 200, child: buildGridViewItems()),
                            ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            child: SizedBox(
                              height: 40,
                              child: TextFormField(
                                controller: _locationController,
                                onChanged: (value) => setState(() {
                                  _post.postLocation = value.toString();
                                }),
                                decoration: const InputDecoration(
                                  errorStyle: TextStyle(height: 0),
                                  labelText: 'เขต/พื้นที่',
                                  hintText: 'เขต/พื้นที่',
                                  hintStyle: TextStyle(fontSize: 12),
                                  border: OutlineInputBorder(),
                                ).copyWith(isDense: true),
                                style: const TextStyle(fontSize: 14),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            child: SizedBox(
                              height: 150,
                              child: TextFormField(
                                controller: _detailController,
                                onChanged: (value) => setState(() {
                                  _post.postDetail = value.toString();
                                }),
                                keyboardType: TextInputType.multiline,
                                minLines: 7,
                                maxLines: 7,
                                decoration: const InputDecoration(
                                  errorStyle: TextStyle(height: 0),
                                  labelText: 'รายละเอียดข้อมูล',
                                  hintText: 'รายละเอียดข้อมูล',
                                  hintStyle: TextStyle(fontSize: 12),
                                  border: OutlineInputBorder(),
                                ).copyWith(isDense: true),
                                style: const TextStyle(fontSize: 14),
                              ),
                            ),
                          ),
                          Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 5),
                              alignment: Alignment.centerLeft,
                              child: const Text('ช่องทางการติดต่อ')),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            child: SizedBox(
                              height: 40,
                              child: TextFormField(
                                controller: _phoneController,
                                onChanged: (value) => setState(() {
                                  _post.postPhone = value.toString();
                                }),
                                decoration: const InputDecoration(
                                  errorStyle: TextStyle(height: 0),
                                  labelText: 'เบอร์โทรศัพท์',
                                  hintText: 'เบอร์โทรศัพท์',
                                  hintStyle: TextStyle(fontSize: 12),
                                  border: OutlineInputBorder(),
                                ).copyWith(isDense: true),
                                style: const TextStyle(fontSize: 14),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            child: SizedBox(
                              height: 40,
                              child: TextFormField(
                                controller: _lineController,
                                onChanged: (value) => setState(() {
                                  _post.postLine = value.toString();
                                }),
                                decoration: const InputDecoration(
                                  errorStyle: TextStyle(height: 0),
                                  labelText: 'Line',
                                  hintText: 'Line',
                                  hintStyle: TextStyle(fontSize: 12),
                                  border: OutlineInputBorder(),
                                ).copyWith(isDense: true),
                                style: const TextStyle(fontSize: 14),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            child: SizedBox(
                              height: 40,
                              child: TextFormField(
                                controller: _facebookController,
                                onChanged: (value) => setState(() {
                                  _post.postFacebook = value.toString();
                                }),
                                decoration: const InputDecoration(
                                  errorStyle: TextStyle(height: 0),
                                  labelText: 'Facebook',
                                  hintText: 'Facebook',
                                  hintStyle: TextStyle(fontSize: 12),
                                  border: OutlineInputBorder(),
                                ).copyWith(isDense: true),
                                style: const TextStyle(fontSize: 14),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            child: SizedBox(
                              height: 40,
                              child: TextFormField(
                                controller: _otherController,
                                onChanged: (value) => setState(() {
                                  _post.postOther = value.toString();
                                }),
                                decoration: const InputDecoration(
                                  errorStyle: TextStyle(height: 0),
                                  labelText: 'อื่นๆ',
                                  hintText: 'อื่นๆ',
                                  hintStyle: TextStyle(fontSize: 12),
                                  border: OutlineInputBorder(),
                                ).copyWith(isDense: true),
                                style: const TextStyle(fontSize: 14),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )),
              ));
  }

  Widget buildGridViewItems() {
    if (_listImage.isNotEmpty) {
      return ListView.builder(
          scrollDirection: Axis.horizontal,
          controller: _scrollController,
          primary: false,
          itemCount: _listImage.length,
          itemBuilder: (context, int index) =>
              listDataItems(_listImage[index], index));
    } else {
      return Container();
    }
  }

  Widget listDataItems(String name, int index) {
    return GestureDetector(
      onTap: () => selectMenu(name, index),
      child: Card(
        child: SizedBox(
          height: 200,
          child: FittedBox(
            child: Image.network("${Globals.API}/uploads/post/$name"),
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }

  Future<void> selectImage(int postImageIndex, bool isNew) async {
    FocusScope.of(context).unfocus();
    await _picker
        .pickImage(source: ImageSource.gallery)
        .then((res) async => await UploadService()
                .uploadImage(res?.path, 'post')
                .then((upload) async {
              var responseData =
                  convert.jsonDecode(await upload!.stream.bytesToString());
              if (upload.statusCode == 200) {
                if (editId != 'null') {
                  PostImageModel postImage = PostImageModel();
                  postImage.imagePostId = editId;
                  postImage.imageThumbnail = responseData['upload_name'];
                  if (isNew) {
                    _listImage.add(responseData['upload_name']);
                    await PostImageService().create(postImage).then((value) =>
                        setState(() => Toasts.toastSuccess(
                            context, 'เพิ่มรูปภาพสำเร็จ', 1)));
                  } else {
                    _listImage[postImageIndex] = responseData['upload_name'];
                    postImage.imageId = _postImage[postImageIndex].imageId;
                    await PostImageService().update(postImage).then((value) =>
                        setState(() => Toasts.toastSuccess(
                            context, 'แก้ไขรูปภาพสำเร็จ', 1)));
                  }
                } else {
                  setState(() => _listImage.add(responseData['upload_name']));
                }
              } else {
                print(responseData);
                Toasts.toastWarning(context, 'เกิดข้อผิดพลาด', 2);
              }
            }).catchError((errUpload) {
              print(errUpload);
              Toasts.toastWarning(context, 'เกิดข้อผิดพลาด', 2);
            }))
        .catchError((err) {
      print(err);
      Toasts.toastWarning(context, 'ไม่ได้เลือกรูปภาพ', 2);
    });
  }

  Future<void> selectMenu(String imageName, int index) async {
    return showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            if (editId != 'null')
              ListTile(
                leading: const Icon(Icons.edit),
                title: const Text('แก้ไข'),
                onTap: () {
                  Navigator.pop(context);
                  selectImage(index, false);
                },
              ),
            ListTile(
              leading: const Icon(Icons.delete),
              title: const Text('ลบ'),
              onTap: () async {
                if (editId != 'null') {
                  await PostImageService()
                      .delete(_postImage[index].imageId ?? '');
                }
                setState(() => _listImage.remove(imageName));
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> onSave() async {
    FocusScope.of(context).unfocus();
    Future.delayed(const Duration(milliseconds: 500), () async {
      if (editId != 'null') {
        await PostService().update(_post).then((value) {
          Toasts.toastSuccess(context, 'แก้ไขข้อมูลสำเร็จ', 1);
          Navigator.pop(context);
        }).catchError((err) {
          print(err);
          Toasts.toastError(context, 'เกิดข้อผิดพลาด', 2);
        });
      } else {
        PostImageModel _postImage = PostImageModel();
        _post.postUserId = _user.userId;
        await PostService().create(_post).then((value) {
          var responseData = convert.jsonDecode(value!.body);
          _postImage.imagePostId = responseData['_id'];
          for (String item in _listImage) {
            _postImage.imageThumbnail = item;
            PostImageService().create(_postImage);
          }
          Toasts.toastSuccess(context, 'เพิ่มข้อมูลสำเร็จ', 1);
          Navigator.pop(context);
        }).catchError((err) {
          print(err);
          Toasts.toastError(context, 'เกิดข้อผิดพลาด', 2);
        });
      }
    });
  }
}

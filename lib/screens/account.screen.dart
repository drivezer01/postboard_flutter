import 'package:flutter/material.dart';
import 'package:postboard_flutter/models/user.model.dart';
import 'package:postboard_flutter/services/auth.service.dart';
import 'package:postboard_flutter/services/user.service.dart';
import 'package:postboard_flutter/utilities/constants.dart';
import 'package:postboard_flutter/utilities/globals.dart';
import 'package:postboard_flutter/widgets/custom-widgets.dart';

import 'edit-profile.screen.dart';
import 'main.screen.dart';

class AccountScreen extends StatefulWidget {
  static const routeName = '/account';

  const AccountScreen({Key? key}) : super(key: key);

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  bool isLoading = true;
  UserModel _user = UserModel();
  String userId = '';

  @override
  void initState() {
    super.initState();
    onInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> onInit() async {
    userId = await AuthService().decodeUserId();
    UserService().findOne(userId).then((res) => setState(() {
          _user = res;
          isLoading = false;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? CustomWidgets.loading(context, 10)
        : Scaffold(
            body: Row(children: [
              Expanded(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: CircleAvatar(
                        radius: 40,
                        backgroundColor: Colors.grey[200],
                        child: CircleAvatar(
                          radius: 37,
                          backgroundColor: Colors.white,
                          backgroundImage: NetworkImage(
                              '${Globals.API}/uploads/profile/${_user.userThumbnail}'),
                        ),
                      ),
                    ),
                    Text('${_user.userFirstname} ${_user.userLastname}'),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width,
                        height: 50,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 40),
                          child: GestureDetector(
                            onTap: () => Navigator.of(context)
                                .pushNamed(EditProfileScreen.routeName)
                                .whenComplete(() => onInit()),
                            child: Card(
                              child: Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  alignment: Alignment.centerLeft,
                                  child: const Text('Edit Profile')),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ]),
            bottomNavigationBar: Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                height: 60,
                child: OutlinedButton(
                    style: Constants.outlineButtonStyle(
                        Colors.redAccent, Colors.redAccent),
                    onPressed: () => onLogout(),
                    child: Container(
                        alignment: Alignment.center,
                        child: const Text('LOGOUT',
                            style: TextStyle(color: Colors.white))))),
          );
  }

  void onLogout() {
    AuthService().removeToken();
    Navigator.of(context).pushReplacementNamed(MainScreen.routeName);
  }
}

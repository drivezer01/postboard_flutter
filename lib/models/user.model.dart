class UserModel {
  String? userId;
  String? userEmail;
  String? userPassword;
  String? userFirstname;
  String? userLastname;
  String? userThumbnail;

  UserModel(
      {this.userId,
      this.userEmail,
      this.userPassword,
      this.userFirstname,
      this.userLastname,
      this.userThumbnail});

  factory UserModel.fromJson(Map<String, dynamic> parsedJson) {
    try {
      return UserModel(
          userId: parsedJson['_id'],
          userEmail: parsedJson['user_email'],
          userPassword: parsedJson['user_password'],
          userFirstname: parsedJson['user_firstname'],
          userLastname: parsedJson['user_lastname'],
          userThumbnail: parsedJson['user_thumbnail'] == ''
              ? 'null.png'
              : parsedJson['user_thumbnail']);
    } catch (ex) {
      print('UserModel ====> $ex');
      throw ('factory UserModel.fromJson ====> $ex');
    }
  }

  Map<String, dynamic> toJson() => {
        '_id': userId,
        'user_email': userEmail,
        'user_password': userPassword,
        'user_firstname': userFirstname,
        'user_lastname': userLastname,
        'user_thumbnail': userThumbnail,
      };
}

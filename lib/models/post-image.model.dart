class PostImageModel {
  String? imageId;
  String? imagePostId;
  String? imageThumbnail;

  PostImageModel({this.imageId, this.imagePostId, this.imageThumbnail});

  factory PostImageModel.fromJson(Map<String, dynamic> parsedJson) {
    try {
      return PostImageModel(
          imageId: parsedJson['_id'],
          imagePostId: parsedJson['image_post_id'],
          imageThumbnail: parsedJson['image_thumbnail']);
    } catch (ex) {
      print('PostImageModel ====> $ex');
      throw ('factory PostImageModel.fromJson ====> $ex');
    }
  }

  Map<String, dynamic> toJson() => {
        '_id': imageId,
        'image_post_id': imagePostId,
        'image_thumbnail': imageThumbnail,
      };
}

class PostModel {
  String? postId;
  String? postUserId;
  String? postCarDetail;
  String? postLocation;
  String? postDetail;
  String? postPhone;
  String? postLine;
  String? postFacebook;
  String? postOther;

  PostModel(
      {this.postId,
      this.postUserId,
      this.postCarDetail,
      this.postLocation,
      this.postDetail,
      this.postPhone,
      this.postLine,
      this.postFacebook,
      this.postOther});

  factory PostModel.fromJson(Map<String, dynamic> parsedJson) {
    try {
      return PostModel(
          postId: parsedJson['_id'],
          postUserId: parsedJson['post_user_id'],
          postCarDetail: parsedJson['post_car_detail'],
          postLocation: parsedJson['post_location'],
          postDetail: parsedJson['post_detail'],
          postPhone: parsedJson['post_phone'],
          postLine: parsedJson['post_line'],
          postFacebook: parsedJson['post_facebook'],
          postOther: parsedJson['post_other']);
    } catch (ex) {
      print('PostModel ====> $ex');
      throw ('factory PostModel.fromJson ====> $ex');
    }
  }

  Map<String, dynamic> toJson() => {
        '_id': postId,
        'post_user_id': postUserId,
        'post_car_detail': postCarDetail,
        'post_location': postLocation,
        'post_detail': postDetail,
        'post_phone': postPhone,
        'post_line': postLine,
        'post_facebook': postFacebook,
        'post_other': postOther,
      };
}

class PostWithUserModel {
  String? postId;
  String? postUserId;
  String? postCarDetail;
  String? postLocation;
  String? postDetail;
  String? postPhone;
  String? postLine;
  String? postFacebook;
  String? postOther;
  String? userEmail;
  String? userFirstname;
  String? userLastname;
  String? userThumbnail;

  PostWithUserModel(
      {this.postId,
      this.postUserId,
      this.postCarDetail,
      this.postLocation,
      this.postDetail,
      this.postPhone,
      this.postLine,
      this.postFacebook,
      this.postOther,
      this.userEmail,
      this.userFirstname,
      this.userLastname,
      this.userThumbnail});

  factory PostWithUserModel.fromJson(Map<String, dynamic> parsedJson) {
    try {
      return PostWithUserModel(
          postId: parsedJson['_id'],
          postUserId: parsedJson['post_user_id'],
          postCarDetail: parsedJson['post_car_detail'],
          postLocation: parsedJson['post_location'],
          postDetail: parsedJson['post_detail'],
          postPhone: parsedJson['post_phone'],
          postLine: parsedJson['post_line'],
          postFacebook: parsedJson['post_facebook'],
          postOther: parsedJson['post_other'],
          userEmail: parsedJson['user_email'],
          userFirstname: parsedJson['user_firstname'],
          userLastname: parsedJson['user_lastname'],
          userThumbnail: parsedJson['user_thumbnail'] == ''
              ? 'null.png'
              : parsedJson['user_thumbnail']);
    } catch (ex) {
      print('PostWithUserModel ====> $ex');
      throw ('factory PostWithUserModel.fromJson ====> $ex');
    }
  }

  Map<String, dynamic> toJson() => {
        '_id': postId,
        'post_user_id': postUserId,
        'post_car_detail': postCarDetail,
        'post_location': postLocation,
        'post_detail': postDetail,
        'post_phone': postPhone,
        'post_line': postLine,
        'post_facebook': postFacebook,
        'post_other': postOther,
        'user_email': userEmail,
        'user_firstname': userFirstname,
        'user_lastname': userLastname,
        'user_thumbnail': userThumbnail,
      };
}

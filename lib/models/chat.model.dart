import 'package:postboard_flutter/models/user.model.dart';

class ChatModel {
  String? chatId;
  String? chatRoom;
  String? chatSenderId;
  String? chatReceivedId;
  String? chatText;

  ChatModel(
      {this.chatId,
      this.chatRoom,
      this.chatSenderId,
      this.chatReceivedId,
      this.chatText});

  factory ChatModel.fromJson(Map<String, dynamic> parsedJson) {
    try {
      return ChatModel(
          chatId: parsedJson['_id'],
          chatRoom: parsedJson['chat_room'],
          chatSenderId: parsedJson['chat_sender_id'],
          chatReceivedId: parsedJson['chat_received_id'],
          chatText: parsedJson['chat_text']);
    } catch (ex) {
      print('ChatModel ====> $ex');
      throw ('factory ChatModel.fromJson ====> $ex');
    }
  }

  Map<String, dynamic> toJson() => {
        '_id': chatId,
        'chat_room': chatRoom,
        'chat_sender_id': chatSenderId,
        'chat_received_id': chatReceivedId,
        'chat_text': chatText,
      };
}

class ChatWithUserModel {
  String? chatText;
  String? chatRoom;
  UserModel? chatSender;
  UserModel? chatReceived;

  ChatWithUserModel(
      {this.chatText, this.chatRoom, this.chatSender, this.chatReceived});

  factory ChatWithUserModel.fromJson(Map<String, dynamic> parsedJson) {
    try {
      return ChatWithUserModel(
          chatText: parsedJson['chat_text'],
          chatRoom: parsedJson['chat_room'],
          chatSender: UserModel.fromJson(parsedJson['chat_sender']),
          chatReceived: UserModel.fromJson(parsedJson['chat_received']));
    } catch (ex) {
      print('ChatWithUserModel ====> $ex');
      throw ('factory ChatWithUserModel.fromJson ====> $ex');
    }
  }

  Map<String, dynamic> toJson() => {
        'chat_text': chatText,
        'chat_room': chatRoom,
        'chat_sender': chatSender,
        'chat_received': chatReceived,
      };
}

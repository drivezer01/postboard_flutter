import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xFFFFF9C4);
  static const Color textFieldColor = Color(0xFFFBC02D);
}

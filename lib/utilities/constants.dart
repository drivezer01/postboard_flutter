import 'package:flutter/material.dart';

class Constants {
  static ButtonStyle outlineButtonStyle(Color? bgColor, Color? borderColor) {
    return OutlinedButton.styleFrom(
      backgroundColor: bgColor ?? Colors.white,
      side: BorderSide(color: borderColor ?? Colors.black, width: 1),
    );
  }
}

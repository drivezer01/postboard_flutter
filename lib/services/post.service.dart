import 'dart:io';
import 'package:postboard_flutter/models/post.model.dart';
import 'package:postboard_flutter/utilities/globals.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class PostService {
  static const className = 'PostService';
  final _baseUrl = '${Globals.API}/posts';

  Future<http.Response?> create(PostModel model) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    var data = model.toJson();
    data.remove('_id');
    return await http.post(Uri.parse(_baseUrl),
        body: convert.jsonEncode(data), headers: headers);
  }

  Future<http.Response> update(PostModel model) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    return await http.put(Uri.parse('$_baseUrl/${model.postId}'),
        body: convert.jsonEncode(model.toJson()), headers: headers);
  }

  Future<http.Response> delete(String postId) async {
    return await http.delete(Uri.parse('$_baseUrl/$postId'));
  }

  Future<PostModel> findOne(String postId) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    var response =
        await http.get(Uri.parse('$_baseUrl/$postId'), headers: headers);
    if (response.statusCode == 200) {
      return PostModel.fromJson(convert.jsonDecode(response.body));
    } else {
      print(
          "$className findOne() failed with status ${response.statusCode}. ${response.body.toString()}");
      return PostModel();
    }
  }

  Future<List<PostWithUserModel>> findAll() async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    var response = await http.get(Uri.parse(_baseUrl), headers: headers);
    if (response.statusCode == 200) {
      return parsePost(response.body);
    } else {
      print(
          "$className findAll() failed with status ${response.statusCode}. ${response.body.toString()}");
      return <PostWithUserModel>[];
    }
  }

  List<PostWithUserModel> parsePost(String responseBody) {
    final parsed =
        convert.jsonDecode(responseBody)["data"].cast<Map<String, dynamic>>();
    return parsed
        .map<PostWithUserModel>((json) => PostWithUserModel.fromJson(json))
        .toList();
  }
}

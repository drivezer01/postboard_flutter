import 'dart:io';
import 'package:postboard_flutter/models/chat.model.dart';
import 'package:postboard_flutter/utilities/globals.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class ChatService {
  static const className = 'ChatService';
  final _baseUrl = '${Globals.API}/chats';

  Future<http.Response?> create(ChatModel model) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    var data = model.toJson();
    data.remove('_id');
    return await http.post(Uri.parse(_baseUrl),
        body: convert.jsonEncode(data), headers: headers);
  }

  Future<http.Response> update(ChatModel model) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    return await http.put(Uri.parse('$_baseUrl/${model.chatId}'),
        body: convert.jsonEncode(model.toJson()), headers: headers);
  }

  Future<http.Response> delete(String chatId) async {
    return await http.delete(Uri.parse('$_baseUrl/$chatId'));
  }

  Future<ChatModel> findOne(String chatId) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    var response =
        await http.get(Uri.parse('$_baseUrl/$chatId'), headers: headers);
    if (response.statusCode == 200) {
      return ChatModel.fromJson(convert.jsonDecode(response.body));
    } else {
      print(
          "$className findOne() failed with status ${response.statusCode}. ${response.body.toString()}");
      return ChatModel();
    }
  }

  Future<List<ChatModel>> findAll(String options) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    var response =
        await http.get(Uri.parse('$_baseUrl?s=$options'), headers: headers);
    if (response.statusCode == 200) {
      return parseChat(response.body);
    } else {
      print(
          "$className findAll() failed with status ${response.statusCode}. ${response.body.toString()}");
      return <ChatModel>[];
    }
  }

  List<ChatModel> parseChat(String responseBody) {
    final parsed =
        convert.jsonDecode(responseBody)["data"].cast<Map<String, dynamic>>();
    return parsed.map<ChatModel>((json) => ChatModel.fromJson(json)).toList();
  }

  Future<List<ChatWithUserModel>> getChatList(String userId) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    var response = await http.get(
        Uri.parse('$_baseUrl/actions/chat-list/$userId'),
        headers: headers);
    if (response.statusCode == 200) {
      return parseChatWithUserModel(response.body);
    } else {
      print(
          "$className getChatList() failed with status ${response.statusCode}. ${response.body.toString()}");
      return <ChatWithUserModel>[];
    }
  }

  List<ChatWithUserModel> parseChatWithUserModel(String responseBody) {
    final parsed =
        convert.jsonDecode(responseBody)["data"].cast<Map<String, dynamic>>();
    return parsed
        .map<ChatWithUserModel>((json) => ChatWithUserModel.fromJson(json))
        .toList();
  }
}

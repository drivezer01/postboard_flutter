import 'dart:io';
import 'package:postboard_flutter/models/post-image.model.dart';
import 'package:postboard_flutter/utilities/globals.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class PostImageService {
  static const className = 'PostImageService';
  final _baseUrl = '${Globals.API}/post-images';

  Future<http.Response?> create(PostImageModel model) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    var data = model.toJson();
    data.remove('_id');
    return await http.post(Uri.parse(_baseUrl),
        body: convert.jsonEncode(data), headers: headers);
  }

  Future<http.Response> update(PostImageModel model) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    return await http.put(Uri.parse('$_baseUrl/${model.imageId}'),
        body: convert.jsonEncode(model.toJson()), headers: headers);
  }

  Future<http.Response> delete(String imageId) async {
    return await http.delete(Uri.parse('$_baseUrl/$imageId'));
  }

  Future<PostImageModel> findOne(String imageId) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    var response =
        await http.get(Uri.parse('$_baseUrl/$imageId'), headers: headers);
    if (response.statusCode == 200) {
      return PostImageModel.fromJson(convert.jsonDecode(response.body));
    } else {
      print(
          "$className findOne() failed with status ${response.statusCode}. ${response.body.toString()}");
      return PostImageModel();
    }
  }

  Future<List<PostImageModel>> findAll(String? options) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    var response =
        await http.get(Uri.parse('$_baseUrl?s=$options'), headers: headers);
    if (response.statusCode == 200) {
      return parsePostImage(response.body);
    } else {
      print(
          "$className findAll() failed with status ${response.statusCode}. ${response.body.toString()}");
      return <PostImageModel>[];
    }
  }

  List<PostImageModel> parsePostImage(String responseBody) {
    final parsed =
        convert.jsonDecode(responseBody)["data"].cast<Map<String, dynamic>>();
    return parsed
        .map<PostImageModel>((json) => PostImageModel.fromJson(json))
        .toList();
  }
}

import 'dart:io';
import 'package:postboard_flutter/models/user.model.dart';
import 'package:postboard_flutter/utilities/globals.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class UserService {
  static const className = 'UserService';
  final _baseUrl = '${Globals.API}/users';

  Future<http.Response?> create(UserModel model) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    var data = model.toJson();
    data.remove('_id');
    return await http.post(Uri.parse(_baseUrl),
        body: convert.jsonEncode(data), headers: headers);
  }

  Future<http.Response> update(UserModel model) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    var data = model.toJson();
    if (model.userPassword == null) {
      data.remove('user_password');
    }
    return await http.put(Uri.parse('$_baseUrl/${model.userId}'),
        body: convert.jsonEncode(data), headers: headers);
  }

  Future<UserModel> findOne(String userId) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    var response =
        await http.get(Uri.parse('$_baseUrl/$userId'), headers: headers);
    if (response.statusCode == 200) {
      return UserModel.fromJson(convert.jsonDecode(response.body));
    } else {
      print(
          "$className findOne() failed with status ${response.statusCode}. ${response.body.toString()}");
      return UserModel();
    }
  }
}

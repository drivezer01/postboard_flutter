import 'package:http/http.dart' as http;

import 'package:postboard_flutter/utilities/globals.dart';

class UploadService {
  static const className = 'UploadService';
  final _baseUrl = '${Globals.API}/uploads';

  Future<http.StreamedResponse?> uploadImage(String? file, String type) async {
    try {
      var request = http.MultipartRequest('POST', Uri.parse('$_baseUrl/$type'));
      request.files.add(await http.MultipartFile.fromPath('image', file ?? ''));
      http.StreamedResponse res = await request.send();
      return res;
    } catch (e) {
      print(
          "$className uploadImage(String file, String type) failed with status ${e.toString()}");
    }
  }
}

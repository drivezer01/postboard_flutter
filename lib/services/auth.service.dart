import 'dart:io';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:postboard_flutter/utilities/globals.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class AuthService {
  final _storage = const FlutterSecureStorage();
  static const className = 'AuthService';
  final _baseUrl = '${Globals.API}/auth';

  Future<http.Response> login(String email, String pass) async {
    var headers = {HttpHeaders.contentTypeHeader: 'application/json'};
    return await http.post(Uri.parse('$_baseUrl/login'),
        body: convert.jsonEncode({'user_email': email, 'user_password': pass}),
        headers: headers);
  }

  void setRemember(String check) {
    _storage.write(key: 'remember', value: check);
  }

  Future<String?> getRemember() async {
    return await _storage.read(key: 'remember');
  }

  void setToken(String token) {
    _storage.write(key: 'token', value: token);
  }

  Future<String?> getToken() async {
    return await _storage.read(key: 'token');
  }

  void removeToken() {
    _storage.deleteAll();
  }

  Map<String, dynamic> _parseJwt(String token) {
    final parts = token.split('.');
    if (parts.length != 3) {
      throw Exception('invalid token');
    }

    final payload = _decodeBase64(parts[1]);

    final payloadMap = convert.jsonDecode(payload);
    if (payloadMap is! Map<String, dynamic>) {
      throw Exception('invalid payload');
    }
    return payloadMap;
  }

  String _decodeBase64(String str) {
    String output = str.replaceAll('-', '+').replaceAll('_', '/');

    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += '==';
        break;
      case 3:
        output += '=';
        break;
      default:
        throw Exception('Illegal base64url string!"');
    }

    return convert.utf8.decode(convert.base64Url.decode(output));
  }

  Future<String> decodeUserId() async {
    var token = await getToken();
    return _parseJwt(token ?? '')['user_id'];
  }
}
